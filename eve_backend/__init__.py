from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import os

db = SQLAlchemy()


def create_app(production=False):
    app = Flask(__name__)

    from instance import config

    if production:
        app.config.from_object(config.ProductionConfig(app))
    else:
        app.config.from_object(config.DevelopmentConfig(app))

    db.init_app(app)

    from eve_backend import models, database

    app.cli.add_command(database.init_database)

    @app.shell_context_processor
    def make_shell_context():
        return {
            "db": db,
            "ParentCategory": models.ParentCategory,
            "WordCategory": models.WordCategory,
            "Word": models.Word,
            "Definition": models.Definition,
            "Exercise": models.Exercise,
        }

    return app
