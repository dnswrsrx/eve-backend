import click
import csv, os
from flask import current_app
from flask.cli import with_appcontext
from eve_backend import db
from eve_backend.models import (
    ParentCategory,
    WordCategory,
    Word,
    Definition,
    Exercise
)


@click.command("init-db")
@with_appcontext
def init_database():
    populate_parent_category()
    populate_word_category()
    populate_word()
    populate_definition()
    populate_exercise()
    db.session.commit()


def populate_parent_category():
    csv_file = os.path.join(current_app.instance_path, "parent_category.csv")
    with open(csv_file, "r", encoding="utf-8") as content:
        table = csv.reader(content)
        for row in table:
            db.session.add(ParentCategory(name=type_csv_data(row[0])))

def populate_word_category():
    csv_file = os.path.join(current_app.instance_path, "word_category.csv")
    with open(csv_file, "r", encoding="utf-8") as content:
        table = csv.reader(content)
        for row in table:
            db.session.add(WordCategory(
                name=type_csv_data(row[0]),
                free=type_csv_data(row[1]),
                parent_category_id=type_csv_data(row[2]),
            ))

def populate_word():
    csv_file = os.path.join(current_app.instance_path, "root_word.csv")
    with open(csv_file, "r", encoding="utf-8") as content:
        table = csv.reader(content)
        for row in table:
            db.session.add(Word(
                word=type_csv_data(row[0]),
                word_category_id=type_csv_data(row[1]),
                group=type_csv_data(row[2]),
            ))

def populate_definition():
    csv_file = os.path.join(current_app.instance_path, "definition.csv")
    with open(csv_file, "r", encoding="utf-8") as content:
        table = csv.reader(content)
        for row in table:
            db.session.add(Definition(
                definition=type_csv_data(row[0]),
                source=type_csv_data(row[1]),
                word_id=type_csv_data(row[2]),
            ))

def populate_exercise():
    csv_file = os.path.join(current_app.instance_path, "exercise.csv")
    with open(csv_file, "r", encoding="utf-8") as content:
        table = csv.reader(content)
        for row in table:
            db.session.add(Exercise(
                sentence=type_csv_data(row[0]),
                answer=type_csv_data(row[1]),
                word_id=type_csv_data(row[2]),
                word_category_id=type_csv_data(row[3]),
                group=type_csv_data(row[4]),
            ))

def type_csv_data(data):
    if data.isdigit():
        return int(data)
    elif data.lower() == "false":
        return False
    elif data.lower() == "true":
        return True
    elif data.lower() == "none":
        return None
    elif data.lower() == "null":
        return None
    else:
        return data
