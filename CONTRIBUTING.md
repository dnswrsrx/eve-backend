# Contributing

## Setting Up Virtual Environment [Optional]

0. Create a virtual environment (named `virtual_environment`) in the Git repository with Python3 `python -m venv virtual_environment`
1. Activate it `source virtual_environment/bin/activate`
2. Now all your pip packages are contained in this virtual env

## Setting Up Formatter and Precommit Hooks

Uses python's pre-commit and prettier.

0. Install pre-commit `pip install pre-commit` and `pre-commit install`
1. Pre-commit will automatically format files prior to commit from now on

## Install required pip packages

0. `pip install -r requirements.txt`
